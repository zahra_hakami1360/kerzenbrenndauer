package z.hakami.kerzenbrenndauer;
/**
 * Kerze
 *
 * @author Zahra Hakami
 * @version 18.05.2021
 */

public class Kerze {
   private double hoehe;
   private double durchmesser;
   private Wachs wachs;


   public Kerze(double hoehe,double durchmesser,Wachs wachs){
      this.hoehe = hoehe;
      this.durchmesser = durchmesser;
      this.wachs = wachs;
   }

   public double getHoehe() {
      return hoehe;
   }

   public void setHoehe(double hoehe) {
      this.hoehe = hoehe;
   }

   public double getDurchmesser() {
      return durchmesser;
   }

   public void setDurchmesser(double durchmesser) {
      this.durchmesser = durchmesser;
   }

   public Wachs getWachs() {
      return wachs;
   }



   public  double berechneZylinderVolumen(){
      double volumen;
      return  volumen = Math.PI* Math.pow(this.durchmesser/2,2)*this.hoehe;
   }
}
