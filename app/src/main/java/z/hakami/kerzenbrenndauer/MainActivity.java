package z.hakami.kerzenbrenndauer;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

/**
 * Aufgabenstellung:
 * Es ist eine Android-App zu erstellen, welche anhand der notwendigen Eingaben die voraussichtliche
 * Brenndauer einer Kerze berechnet.
 * Dabei sollen folgende Anforderungen erfüllt werden:
 *  Die notwenigen Modelklassen sind zu erstellen.
 *  Die App soll nur eine Activity enthalten
 *  Das Layout und die Formatierungen sind frei wählbar.
 * Der dargestellte Screenshot ist nur ein Beispiel.
 *  Das Material soll aus einer Liste ausgewählt werden können.
 *  Es sollen mehrere Sprachen (mind. 2) unterstützt werden.
 * Die Sprachen sind frei wählbar.
 *  Die App soll robust gegen Fehleingaben sein.
 *  Die Clean Code Regeln sind einzuhalten.
 * Die Aufgabe soll nicht in Gruppenarbeit oder Pair-/Mob-Programming erstellt werden.
 * Für die Aufgabe soll ein neues Projekt angelegt werden.
 * Abzugeben ist das komplette Projekt als Archiv (zip oder 7z) über Teams
 * <p>
 * * @version 18.05.2021
 *
 * @autor Zahra Hakami
 */
public class MainActivity extends AppCompatActivity {
    private Spinner spinnerListWachs;
    private EditText editTextLaenge;
    private EditText editTextDurchmesser;
    private TextView textViewBrenndauer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //spinner mit xml datei bauen
        //Einträge über List
        List<Wachs> wachsList = new LinkedList<>();
        wachsList.add(new Wachs("Paraffinwachs ", 7.5, 0.90));
        wachsList.add(new Wachs("Stearin  ", 6.5, 0.93));
        wachsList.add(new Wachs("Bienenwachs  ", 4.0, 0.95));

        spinnerListWachs = findViewById(R.id.spinnerListWachs);
        spinnerListWachs.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, wachsList));
    }

    public void onClick_berechnen(View view) {
        // Kerze aus den eingegebenen Daten bauen

        editTextLaenge = findViewById(R.id.editTextLaenge);
        String eingabe = editTextLaenge.getText().toString();
        double laenge = Double.parseDouble(eingabe);
        Log.i("input länge", String.valueOf(laenge));


        editTextDurchmesser = findViewById(R.id.editTextDurchmesser);
        String eingabeDurchmesser = editTextDurchmesser.getText().toString();
        double durchmesser = Double.parseDouble(eingabeDurchmesser);
        Log.i("input durchmesser", String.valueOf(durchmesser));


        spinnerListWachs = findViewById(R.id.spinnerListWachs);
        Wachs wachs = (Wachs) spinnerListWachs.getSelectedItem();
        Log.i("input Wachs", wachs.toString());

        // Berechnen
        Kerze kerze = new Kerze(laenge, durchmesser, wachs);
        double volumen = kerze.berechneZylinderVolumen();
        Log.i("input Volumen", String.valueOf(volumen));

        //gewicht =  volumen * kerze.getwachs().getdichte
        double gewicht = volumen * kerze.getWachs().getDichte();
        Log.i("input gewicht", String.valueOf(gewicht));

        //brenndauer rechnen
        double brenndauer = gewicht / kerze.getWachs().getBrenngeschwindigkeit();
        Log.i("input brenndauer", String.valueOf(brenndauer));

        // Konvertion Min in Stunde:
        int hours = (int) brenndauer;
        Log.i("input hours", String.valueOf(hours));
        int minutes = (int) Math.round((brenndauer - hours) * 60);
        Log.i("input minutes", String.valueOf(minutes));

        // Die berechnete Zeit versetzt den Kerzenbrennstoff in die Kerzenbrennstoffperiode
        //leer varibel für ausgabe
        String ungefaereBrenndauer = "";
        textViewBrenndauer = findViewById(R.id.textViewBrenndauer);
        ungefaereBrenndauer = hours + ":" + minutes;
        Log.i("input minutes", ungefaereBrenndauer);
        //
        textViewBrenndauer.setText(ungefaereBrenndauer);
    }

    public void onClickResetButten(View view) {
        loeschenView();
    }

    private void loeschenView() {
         spinnerListWachs.setSelection(0);
         editTextLaenge.setText("");
         editTextDurchmesser.setText("");
         textViewBrenndauer.setText("");

    }
}