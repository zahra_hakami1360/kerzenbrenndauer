package z.hakami.kerzenbrenndauer;
/**
 * Wachs
 *
 * @author Zahra Hakami
 * @version 18.05.2021
 */

public class Wachs {
    private String name;
    private double brenngeschwindigkeit;
    private double dichte;

    public Wachs(String name , double brenngeschwindigkeit , double dichte){
        this.name = name;
        this.brenngeschwindigkeit = brenngeschwindigkeit;
        this.dichte = dichte;
    }

    @Override
    public String toString() {
        return name  ;
    }

    public String getName() {
        return name;
    }

    public double getBrenngeschwindigkeit() {
        return brenngeschwindigkeit;
    }

    public double getDichte() {
        return dichte;
    }


}
